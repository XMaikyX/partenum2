﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BaseUleam.Migrations
{
    public partial class NewDataBase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.CreateTable(
            //    name: "Administradors",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        Nombre = table.Column<string>(nullable: true),
            //        Apellido = table.Column<string>(nullable: true),
            //        Contrasenia = table.Column<string>(nullable: true),
            //        Correo = table.Column<string>(nullable: true),
            //        Cedula = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Administradors", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Alumnoss",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        Nombre = table.Column<string>(nullable: true),
            //        Apellido = table.Column<string>(nullable: true),
            //        Nivel = table.Column<string>(nullable: true),
            //        Matricula = table.Column<string>(nullable: true),
            //        Correo = table.Column<string>(nullable: true),
            //        Contrasenia = table.Column<string>(nullable: true),
            //        Cedula = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Alumnoss", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Aulas",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        NombreAula = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Aulas", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Docentess",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        Nombre = table.Column<string>(nullable: true),
            //        Apellido = table.Column<string>(nullable: true),
            //        Asignatura = table.Column<string>(nullable: true),
            //        Contrasenia = table.Column<string>(nullable: true),
            //        Correo = table.Column<string>(nullable: true),
            //        Cedula = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Docentess", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "logins",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        Nombre = table.Column<string>(nullable: true),
            //        Apellido = table.Column<string>(nullable: true),
            //        Nombre_Usuario = table.Column<string>(nullable: true),
            //        _Contrasenia = table.Column<string>(nullable: true),
            //        TipoUsuario = table.Column<string>(nullable: true),
            //        Ced_Identidad = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_logins", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "menus",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_menus", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "SeleccionarTutorias",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        Fecha = table.Column<DateTime>(nullable: false),
            //        Aula = table.Column<string>(nullable: true),
            //        Asignatura = table.Column<string>(nullable: true),
            //        Tema = table.Column<string>(nullable: true),
            //        Docente = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_SeleccionarTutorias", x => x.ID);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "tipousuarios",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        Docente = table.Column<string>(nullable: true),
            //        Alumno = table.Column<string>(nullable: true),
            //        Administrador = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_tipousuarios", x => x.Id);
            //    });

            migrationBuilder.CreateTable(
                name: "Tutorias",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Fecha = table.Column<DateTime>(nullable: false),
                    Aula = table.Column<string>(nullable: true),
                    Asignatura = table.Column<string>(nullable: true),
                    Tema = table.Column<string>(nullable: true),
                    FechaCreacion = table.Column<DateTime>(nullable: false),
                    Docente = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tutorias", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropTable(
            //    name: "Administradors");

            //migrationBuilder.DropTable(
            //    name: "Alumnoss");

            //migrationBuilder.DropTable(
            //    name: "Aulas");

            //migrationBuilder.DropTable(
            //    name: "Docentess");

            //migrationBuilder.DropTable(
            //    name: "logins");

            //migrationBuilder.DropTable(
            //    name: "menus");

            //migrationBuilder.DropTable(
            //    name: "SeleccionarTutorias");

            //migrationBuilder.DropTable(
            //    name: "tipousuarios");

            migrationBuilder.DropTable(
                name: "Tutorias");
        }
    }
}
