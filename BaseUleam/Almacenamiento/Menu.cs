﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseUleam.Almacenamiento
{
    class Menu
    {
        public int Id { get; set; }
        public void Desplegar()
        {
            int opcion1, opcion2, opcion3;


            do
            {
                Console.ForegroundColor = ConsoleColor.Green;

                Console.Clear();
                Console.WriteLine("====================UNIVERSIDAD LAICA ELOY ALFARO DE MANABÍ=================");
                Console.WriteLine("=========FACULTAD DE CIENCIAS INFORMÁTICAS=======TECNOLOGÍAS DE LA INFORMACIÓN======");
                Console.WriteLine("SISTEMA DE CONTROL DE REGISTRO DE TUTORÍAS ACADÉMICAS");
                Console.WriteLine("Bienvenido");
                Console.WriteLine("Como que tipo de Usuario desea ingresar");
                Console.WriteLine("\n 1.- Iniciar Sesion\n" +
                                  "\n 2.- Registrarse\n" +
                                  "\n 3.- Salir \n Opcion:  ");

                opcion1 = Convert.ToInt32(Console.ReadLine());
                switch (opcion1)
                {
                    case 1:
                        do
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.Clear();

                            Console.WriteLine("================================");
                            Console.WriteLine(" 1.- Iniciar Sesión \n" +
                                              " 2.- Regresar");
                            opcion2 = Convert.ToInt32(Console.ReadLine());
                            switch (opcion2)
                            {
                                case 1:
                                    //LoginAdministrador();
                                    //LoginEstudiante();
                                    LoginDocente();
                                    //ListaDeTutorias();
                                    Console.WriteLine("Presione la tecla ENTER para continuar");
                                    Console.ReadLine();
                                    break;
                                case 2:
                                    break;
                            }
                        } while (opcion2 != 2);
                        break;

                    case 2:
                        do
                        {
                            Console.ForegroundColor = ConsoleColor.Blue;
                            Console.Clear();
                            Console.WriteLine("Bienvenido a la ventana de Registro de Usuario");
                            Console.WriteLine("\n Elija el Usuario a Registrarse \n" +
                                              " 1.- Docente \n" +
                                              " 2.- Alumno \n" +
                                              " 3.- Administrador \n" +
                                              " 4.- Salir");
                            opcion3 = Convert.ToInt32(Console.ReadLine());
                            switch (opcion3)
                            {
                                case 1:
                                    Console.Clear();
                                    Console.WriteLine("Bienvenido a la ventana de Registro de Usuario Docente");
                                    IngresarProfesor();
                                    Console.WriteLine(" *********** Se ha registrado satisfactoriamente *********** ");
                                    Console.WriteLine("Presione la tecla ENTER para continuar");
                                    //CrearRegistroUsuario();
                                    //IngresarAsignaturas();
                                    //ListaAulas();
                                    break;
                                case 2:
                                    //do
                                    //{
                                    Console.Clear();
                                    Console.WriteLine("Bienvenido a la ventana de Registro de Usuario Alumno");
                                    IngresarAlumno();
                                    Console.WriteLine(" *********** Se ha registrado satisfactoriamente *********** ");
                                    Console.WriteLine("Presione la tecla ENTER para continuar");
                                    break;
                                case 3:
                                    Console.Clear();
                                    Console.WriteLine("Bienvenido a la ventana de Registro de Usuario Administrador");
                                    IngresarAdministrador();
                                    Console.WriteLine(" *********** Se ha registrado satisfactoriamente *********** ");
                                    //CrearRegistroUsuario();
                                    //IngresarAulas();
                                    Console.WriteLine("Presione la tecla ENTER para continuar");
                                    break;
                                case 4:
                                    break;
                            }
                        } while (opcion3 != 4);
                        break;

                }
            } while (opcion1 != 3);
        }

        static void ListadoProfesores()
        {
            using (var db = new EntityFrameworkContext())
            {
                List<Docentes> listadoProfesores = db.Docentess.ToList();
                foreach (var profesor in listadoProfesores)
                {
                    Console.WriteLine("Apellido:" + profesor.Apellido + "Nombre" + profesor.Nombre);
                }
            }
        }
        static void RegistroUsuario()
        {
            using (var db= new EntityFrameworkContext())
            {
                Login login = new Login();
                Console.WriteLine("Ingrese el Nombre de Usuario");
                login.Nombre_Usuario = Console.ReadLine();
                Console.WriteLine("Ingrese una Contraseña");
                login._Contrasenia = Console.ReadLine();
                Console.WriteLine("Ingrese el tipo de Usuario");
                login.TipoUsuario = Console.ReadLine();
                Console.WriteLine("Ingrese su Número de Cédula");
                login.Ced_Identidad = Console.ReadLine();
            }
        }
        static void IngresarProfesor()
        {
            using (var db = new EntityFrameworkContext())
            {
                Docentes docentes = new Docentes();
                Login login = new Login();
                Console.WriteLine("Ingrese el nombre del docente");
                docentes.Nombre = Console.ReadLine();
                login.Nombre = docentes.Nombre;
                Console.WriteLine("Ingrese el apellido del docente");
                docentes.Apellido = Console.ReadLine();
                login.Apellido = docentes.Apellido;
                Console.WriteLine("Ingrese su numero de cedula");
                docentes.Cedula = Console.ReadLine();
                login.Ced_Identidad = docentes.Cedula;
                Console.WriteLine("Su correo es el siguiente");
                docentes.Correo = "p" + docentes.Cedula + "@uleam.edu.ec";
                login.Nombre_Usuario = docentes.Correo;
                Console.WriteLine(docentes.Correo);
                Console.WriteLine("Asigne una contraseña a su cuenta");
                docentes.Contrasenia = Console.ReadLine();
                login._Contrasenia = docentes.Contrasenia;
                login.TipoUsuario = "Docente";

                Console.WriteLine("Se ha creado su usuario Correctamente");
                db.Add(docentes);
                db.Add(login);
                db.SaveChanges();

            }
        }
        static void IngresarAdministrador()
        {
            using (var db = new EntityFrameworkContext())
            {
                Administrador administrador = new Administrador();
                Login login = new Login();

                Console.WriteLine("Ingrese el nombre del docente");
                administrador.Nombre = Console.ReadLine();
                login.Nombre = administrador.Nombre;
                Console.WriteLine("Ingrese el apellido del docente");
                administrador.Apellido = Console.ReadLine();
                login.Apellido = administrador.Apellido;
                Console.WriteLine("Ingrese su numero de cedula");
                administrador.Cedula = Console.ReadLine();
                login.Ced_Identidad = administrador.Cedula;
                Console.WriteLine("Su correo es el siguiente");
                administrador.Correo = "ad" + administrador.Cedula + "@uleam.edu.ec";
                login.Nombre_Usuario = administrador.Correo;
                Console.WriteLine(administrador.Correo);
                Console.WriteLine("Asigne una contraseña a su cuenta");
                administrador.Contrasenia = Console.ReadLine();
                login._Contrasenia = administrador.Contrasenia;


                db.Add(administrador);
                db.Add(login);
                db.SaveChanges();

            }
            return;
        }
        static void IngresarAlumno()
        {
            using (var db = new EntityFrameworkContext())
            {
                Alumnos alumnos = new Alumnos();
                Login login = new Login();
                Console.WriteLine("Ingrese el nombre ");
                alumnos.Nombre = Console.ReadLine();
                login.Nombre = alumnos.Nombre;
                Console.WriteLine("Ingrese el apellido");
                alumnos.Apellido = Console.ReadLine();
                login.Apellido = alumnos.Apellido;
                Console.WriteLine("Ingrese su numero de cedula");
                alumnos.Cedula = Console.ReadLine();
                login.Ced_Identidad = alumnos.Cedula;
                Console.WriteLine("Ingrese su nivel cursado actualmente" +
                    " ======== 1 - 2 - 3 - 4 - 5 - 6 - 7 - 8 - 9 - 10 ========");
                alumnos.Nivel = Console.ReadLine();
                Console.WriteLine("Ingrese su matricula (Primera, Segunda o Tercera matricula)");
                alumnos.Matricula = Console.ReadLine();
                Console.WriteLine("Su correo es el siguiente");
                alumnos.Correo = "e" + alumnos.Cedula + "@uleam.edu.ec";
                login.Nombre_Usuario = alumnos.Correo;
                Console.WriteLine(alumnos.Correo);
                Console.WriteLine("Asigne una contraseña a su cuenta");
                alumnos.Contrasenia = Console.ReadLine();
                login._Contrasenia = alumnos.Contrasenia;
                //Console.WriteLine(" Escriba el Tipo de Usuario");
                //alumnos.TipoUsuario = "Alumno";
                //int Opcion;
                ////************************
                //do
                //{

                //    Console.WriteLine("Elija el tipo de usuario");
                //    Console.WriteLine("\n 1.- Docente \n" +
                //                      "\n 2.- Alumno \n" +
                //                      "\n 3.- Administrador" +
                //                      "\n 4.- Salir");
                //    Opcion = Convert.ToInt32(Console.ReadLine());
                //    switch (Opcion)
                //    {
                //        case 1:
                //            Tipousuario usuario = new Tipousuario();
                //            Console.WriteLine("El usuario ha elegido el tipo de usuario" + usuario.Administrador);
                //            Console.ReadLine();
                //            break;
                //        case 2:
                //            Tipousuario Usuario = new Tipousuario();
                //            Console.WriteLine("El usuario ha elegido el tipo de usuario" + Usuario.Alumno);
                //            Console.ReadLine();
                //            break;
                //        case 3:
                //            Tipousuario _Usuario = new Tipousuario();
                //            Console.WriteLine("El usuario ha elegido el tipo de usuario" + _Usuario.Docente);
                //            Console.ReadLine();
                //            break;
                //    }
                //    break;


                //} while (Opcion != 4);


                db.Add(alumnos);
                db.Add(login);
                db.SaveChanges();

            }
            return;
        }

        static void ListaAulas()
        {
            using (var db = new EntityFrameworkContext())
            {
                List<Aula> ListaAulas = db.Aulas.ToList();
                foreach (var aulas in ListaAulas)
                {
                    Console.WriteLine(aulas.Id +".  "+ aulas.NombreAula);
                }
            }
        }
        static void IngresarAulas()
        {
            using (var db = new EntityFrameworkContext())
            {
                Aula aula = new Aula();
                Console.WriteLine("Ingrese el nombre del Aula");
                aula.NombreAula = Console.ReadLine();
                Console.WriteLine(aula.NombreAula);

                db.Add(aula);
                db.SaveChanges();

            }
            return;
        }
        static void CrearTutoria()
        {
            using (var db = new EntityFrameworkContext())
            {
                Docentes docentes = new Docentes();
                Tutoria tutoria = new Tutoria();

                DateTime fecha = DateTime.Now;
                Console.WriteLine("Proceda a escribir la fecha y hora de la Tutoria");
                Console.WriteLine("Escriba el Año ");
                string anio = Console.ReadLine();
                Console.WriteLine("Escriba el Mes");
                string mes = Console.ReadLine();
                Console.WriteLine("Escriba el Dia");
                string dia = Console.ReadLine();
                Console.WriteLine("Escriba la hora");
                string hora = Console.ReadLine();
                DateTime crearfecha = new DateTime(int.Parse(anio), int.Parse(mes), int.Parse(dia), int.Parse(hora), 0, 0);
                tutoria.Fecha = crearfecha;

                Console.WriteLine("Ingrese el Aula a utilizar");
                tutoria.Aula = Console.ReadLine();
                Console.WriteLine("Ingrese la Asignatura");
                tutoria.Asignatura = Console.ReadLine();
                Console.WriteLine("Ingrese el Tema");
                tutoria.Tema = Console.ReadLine();
                Console.WriteLine("\nSe asignó tutoría para:");
                tutoria.FechaCreacion = DateTime.Now;
                tutoria.Docente = docentes.Nombre;
                Console.WriteLine(tutoria.Fecha + " " + tutoria.Aula + " " + tutoria.Asignatura + "\n" + tutoria.Tema + "    Fecha de Creacion " + tutoria.FechaCreacion);

                db.Add(tutoria);
                db.SaveChanges();
            }
            return;
        }

        static void ListaDeTutorias()
        {
            using (var db = new EntityFrameworkContext())
            {
                List<Tutoria> ListaDeTutorias = db.Tutorias.ToList();
                foreach (var Tutoria in ListaDeTutorias)
                {

                    Console.WriteLine("Fecha: " + Tutoria.Fecha+" "+Tutoria.Aula+" "+Tutoria.Asignatura+" "+Tutoria.Tema);
                }
            }
        }
        static void ListaDeTutoriaestudiante()
        {
            using (var db = new EntityFrameworkContext())
            {
                List<Tutoria> ListaDeTutorias = db.Tutorias.ToList();
                foreach (var Tutoria in ListaDeTutorias)
                {

                    Console.WriteLine(Tutoria.ID+"Fecha: " + Tutoria.Fecha + Tutoria.Aula + Tutoria.Asignatura + Tutoria.Tema+"DOCENTE"+ Tutoria.Docente);
                }
            }
        }
        static void SeleccionarTutoria()    
        {
            using (var db = new EntityFrameworkContext())
            {
                SeleccionarTutoria seleccionarTutoria = new SeleccionarTutoria();
                Console.WriteLine("Seleccione la tutoria a la que se quiee integrar");
                var tutoria = (from resultado in db.Tutorias
                               where resultado.ID == Convert.ToInt32(Console.ReadLine())
                               select resultado).FirstOrDefault<Tutoria>();
                seleccionarTutoria.Fecha = tutoria.Fecha;
                seleccionarTutoria.Aula = tutoria.Aula;
                seleccionarTutoria.Docente = tutoria.Docente;
                seleccionarTutoria.Asignatura = tutoria.Asignatura;
                seleccionarTutoria.Tema = tutoria.Tema;
                //seleccionarTutoria.Estudiante = alumnos.Nombre;

                db.Add(seleccionarTutoria);
                db.SaveChanges();

            }
        }
        static void ListaDeTutoriasAgregadas()
        {
            using (var db = new EntityFrameworkContext())
            {
                var seleccionarLista = (from resultado in db.SeleccionarTutorias
                               where resultado.Estudiante =="NOmbre"
                               select resultado).FirstOrDefault<SeleccionarTutoria>();
                List<SeleccionarTutoria> SeleccionDeTutorias = db.SeleccionarTutorias.ToList();
                foreach (var seleccionarTutoria in SeleccionDeTutorias)
                {
                    Console.WriteLine("Fecha: " + seleccionarTutoria.Fecha + " " + seleccionarTutoria.Aula + " " + seleccionarTutoria.Asignatura + " " +seleccionarTutoria.Tema); ;
                }
            }
        }
        static void IngresarAsignaturas()
        {
            using (var db = new EntityFrameworkContext())
            {
                Docentes docentes = new Docentes();
                Console.WriteLine("Ingrese la/las asignatura/s");
                docentes.Asignatura = Console.ReadLine();
                Console.WriteLine(docentes.Asignatura, docentes.Nombre, docentes.Apellido);
                db.Add(docentes);
                db.SaveChanges();
            }
            return;
        }
        static void ListaDeAsignaturas()
        {
            using (var db = new EntityFrameworkContext())
            {
                List<Docentes> ListaDeAsignaturas = db.Docentess.ToList();
                foreach (var Docentes in ListaDeAsignaturas)
                {
                    Console.WriteLine("Asignatura/s: " + Docentes.Asignatura);
                }
            }
        }
        //=============================

        static void LoginEstudiante()
        {
            using (var db = new EntityFrameworkContext())
            {
                Console.WriteLine("Escriba el Correo del usuario");
                var alumnos = (from resultado in db.Alumnoss
                               where resultado.Correo == Console.ReadLine()
                               select resultado).FirstOrDefault<Alumnos>();
                Console.WriteLine("Escriba la contraseña del usuario");
                ConsoleKeyInfo ultimaTecla;
                bool continuar;
                char mostrar;
                char[] cadena = new char[100];
                int i = 0;
                continuar = true;
                mostrar = '*';
                string contraseniaIngresada = "";
                while (continuar)
                {

                    ultimaTecla = Console.ReadKey(true);

                    if (ultimaTecla.KeyChar != 13)
                    {

                        if (ultimaTecla.KeyChar != 8)
                        {
                            cadena[i] = ultimaTecla.KeyChar;
                            i++;
                            Console.Write(mostrar);
                        }
                        else
                        {
                            cadena[i] = '\0';
                            i--;
                            Console.Write("\b \b");
                            continuar = true;
                        }

                    }
                    else
                        continuar = false;
                }
                cadena[i] = '\0';

                for (int j = 0; j < cadena.Length; j++)
                {
                    if (cadena[j].Equals('\0'))
                    {
                        break;
                    }
                    else
                    {
                        contraseniaIngresada = contraseniaIngresada + cadena[j];
                    }

                }
                if (alumnos.Contrasenia == contraseniaIngresada)
                {
                    Console.WriteLine(" Bienvenido {0} {1} {2} ", alumnos.Apellido, alumnos.Nombre, alumnos.Cedula);
                    ListaDeTutoriaestudiante();
                    {
                        SeleccionarTutoria seleccionarTutoria = new SeleccionarTutoria();
                        Console.WriteLine("Seleccione la tutoria a la que se quiee integrar");
                        var tutoria = (from resultado in db.Tutorias
                                       where resultado.ID == Convert.ToInt32(Console.ReadLine())
                                       select resultado).FirstOrDefault<Tutoria>();
                        seleccionarTutoria.Fecha = tutoria.Fecha;
                        seleccionarTutoria.Aula = tutoria.Aula;
                        seleccionarTutoria.Docente = tutoria.Docente;
                        seleccionarTutoria.Asignatura = tutoria.Asignatura;
                        seleccionarTutoria.Tema = tutoria.Tema;
                        seleccionarTutoria.Estudiante = alumnos.Nombre;

                        db.Add(seleccionarTutoria);
                        db.SaveChanges();

                    }
                    Console.ReadLine();
                    
                }
                else
                {
                    Console.WriteLine("No existe usuario");
                    LoginEstudiante();

                }
            }
        }

        static void LoginDocente()
        {
            using (var db = new EntityFrameworkContext())
            {
                Console.WriteLine("Escriba el Correo del usuario");
                var docente = (from resultado in db.Docentess
                               where resultado.Correo == Console.ReadLine()
                               select resultado).FirstOrDefault<Docentes>();
                Console.WriteLine("Escriba la contraseña del usuario");
                {
                    ConsoleKeyInfo ultimaTecla;
                    bool continuar;
                    char mostrar;
                    char[] cadena = new char[100];
                    int i = 0;
                    continuar = true;
                    mostrar = '*';
                    string contraseniaIngresada = "";
                    while (continuar)
                    {

                        ultimaTecla = Console.ReadKey(true);

                        if (ultimaTecla.KeyChar != 13)
                        {

                            if (ultimaTecla.KeyChar != 8)
                            {
                                cadena[i] = ultimaTecla.KeyChar;
                                i++;
                                Console.Write(mostrar);
                            }
                            else
                            {
                                cadena[i] = '\0';
                                i--;
                                Console.Write("\b \b");
                                continuar = true;
                            }

                        }
                        else
                            continuar = false;
                    }
                    cadena[i] = '\0';

                    for (int j = 0; j < cadena.Length; j++)
                    {
                        if (cadena[j].Equals('\0'))
                        {
                            break;
                        }
                        else
                        {
                            contraseniaIngresada = contraseniaIngresada + cadena[j];
                        }

                    }

                    if (docente.Contrasenia == contraseniaIngresada)
                    {
                        Console.WriteLine(" Bienvenido {0} {1} {2} ", docente.Apellido, docente.Nombre, docente.Cedula);

                        {
                            var tutoria = (from resultado in db.Tutorias
                                           where resultado.Docente == docente.Nombre
                                           select resultado).FirstOrDefault<Tutoria>();
                            //{
                            //    List<Tutoria> ListaDeTutorias = db.Tutorias.ToList();
                            foreach (var Tutoria in tutoria)
                            {
                                Console.WriteLine("Fecha: " + tutoria.Fecha + " " + tutoria.Aula + " " + tutoria.Asignatura + " " + tutoria.Tema);
                            }
                            //}
                        }
                        {

                            Tutoria tutoria = new Tutoria();

                            DateTime fecha = DateTime.Now;
                            Console.WriteLine("Proceda a escribir la fecha y hora de la Tutoria");
                            Console.WriteLine("Escriba el Año ");
                            string anio = Console.ReadLine();
                            Console.WriteLine("Escriba el Mes");
                            string mes = Console.ReadLine();
                            Console.WriteLine("Escriba el Dia");
                            string dia = Console.ReadLine();
                            Console.WriteLine("Escriba la hora");
                            string hora = Console.ReadLine();
                            DateTime crearfecha = new DateTime(int.Parse(anio), int.Parse(mes), int.Parse(dia), int.Parse(hora), 0, 0);
                            tutoria.Fecha = crearfecha;

                            Console.WriteLine("Digite el Aula a utilizar");
                            ListaAulas();
                            var aulas = (from resultado in db.Aulas
                                         where resultado.Id == Convert.ToInt32(Console.ReadLine())
                                         select resultado).FirstOrDefault<Aula>();
                            tutoria.Aula = aulas.NombreAula;
                            Console.WriteLine("Ingrese la Asignatura");
                            tutoria.Asignatura = Console.ReadLine();
                            Console.WriteLine("Ingrese el Tema");
                            tutoria.Tema = Console.ReadLine();
                            Console.WriteLine("\nSe asignó tutoría para:");
                            tutoria.FechaCreacion = DateTime.Now;
                            tutoria.Docente = docente.Nombre + docente.Apellido;
                            Console.WriteLine(tutoria.Fecha + " " + tutoria.Aula + " " + tutoria.Asignatura + "\n" + tutoria.Tema + "    Fecha de Creacion " + tutoria.FechaCreacion);

                            db.Add(tutoria);
                            db.SaveChanges();
                        }
                        return;


                    }
                    else
                    {
                        Console.WriteLine("No existe usuario");
                        LoginEstudiante();
                    }

                }
            }
        }
        static void Login()
        {
            using (var db = new EntityFrameworkContext())
            {
                Console.WriteLine(" Escriba el Correo del Usuario");
                var login = (from resultado in db.logins 
                             where resultado.Nombre_Usuario  == Console.ReadLine()
                             select resultado).FirstOrDefault<Login>();

                //var docente = (from resultado in db.Docentess
                //               where resultado.Correo == login.Nombre_Usuario
                //               select resultado).FirstOrDefault<Docentes>();
                //var estudiante = (from resultado in db.Alumnoss
                //                  where resultado.Correo == login.Nombre_Usuario
                //                  select resultado).FirstOrDefault<Alumnos>();
                //var administrador = (from resultado in db.Administradors
                //                     where resultado.Correo == login.Nombre_Usuario
                //                     select resultado).FirstOrDefault<Administrador>();
                Console.WriteLine("Escriba la contraseña del usuario");
                if (login._Contrasenia == Console.ReadLine())
                {
                    Console.WriteLine(" Bienvenido Docente {0} {1} {2} ", login.Apellido, login.Nombre, login.Ced_Identidad);
                    //if (login.Ced_Identidad == tipousuario.Docente)
                    //{
                    //    CrearTutoria();
                    //    Console.ReadLine();
                    //}
                    //else
                    //{
                    //    Console.WriteLine("no existe usuario");
                    //if (login.Ced_Identidad == tipousuario.Alumno)
                    //{
                    //    ListaDeTutorias();
                    //    Console.ReadLine();
                    //}
                    //else
                    //{
                    //    ListaAulas();
                    //    ListaDeTutorias();
                    //    ListadoProfesores();

                    //}
                    //}
                }

                else
                {
                    Console.WriteLine("No existe usuario");
                    LoginEstudiante();
                }
            }
        }
        static void LoginAdministrador()
        {
            using (var db = new EntityFrameworkContext())
            {
                Console.WriteLine("Escriba el Correo del usuario");
                var administrador = (from resultado in db.Administradors
                                     where resultado.Correo == Console.ReadLine()
                                     select resultado).FirstOrDefault<Administrador>();
                Console.WriteLine("Escriba la contraseña del usuario");
                ConsoleKeyInfo ultimaTecla;
                bool continuar;
                char mostrar;
                char[] cadena = new char[100];
                int i = 0;
                continuar = true;
                mostrar = '*';
                string contraseniaIngresada = "";
                while (continuar)
                {

                    ultimaTecla = Console.ReadKey(true);

                    if (ultimaTecla.KeyChar != 13)
                    {

                        if (ultimaTecla.KeyChar != 8)
                        {
                            cadena[i] = ultimaTecla.KeyChar;
                            i++;
                            Console.Write(mostrar);
                        }
                        else
                        {
                            cadena[i] = '\0';
                            i--;
                            Console.Write("\b \b");
                            continuar = true;
                        }

                    }
                    else
                        continuar = false;
                }
                cadena[i] = '\0';

                for (int j = 0; j < cadena.Length; j++)
                {
                    if (cadena[j].Equals('\0'))
                    {
                        break;
                    }
                    else
                    {
                        contraseniaIngresada = contraseniaIngresada + cadena[j];
                    }

                }
                if (administrador.Contrasenia == contraseniaIngresada)
                {
                    
                    Console.WriteLine(" Bienvenido {0} {1} {2} ", administrador.Apellido, administrador.Nombre, administrador.Cedula);
                    {
                        SeleccionarTutoria seleccionarTutoria = new SeleccionarTutoria();
                        List<Tutoria> Tutorias = db.Tutorias.ToList();
                        foreach (var seleccionar in Tutorias)
                        {
                            Console.WriteLine(seleccionar.ID+".  Fecha: " + seleccionar.Fecha + " " + seleccionar.Aula + " " + seleccionar.Asignatura + " " + seleccionar.Tema+"  "+seleccionar.Docente);
                        }
                        Console.WriteLine("Seleccione la tutoria a vizualiza");
                        var seleccionartuto = (from resultado in db.Tutorias
                                             where resultado.ID == Convert.ToInt32(Console.ReadLine())
                                             select resultado).FirstOrDefault<Tutoria>();
                        Console.WriteLine("Fecha: " + seleccionartuto.Fecha + " " + seleccionartuto.Aula + " " + seleccionartuto.Asignatura + " " + seleccionartuto.Tema+"  "+seleccionarTutoria.Docente);
                        if (seleccionartuto.Asignatura == seleccionarTutoria.Asignatura)
                        {
                            Console.WriteLine("Lista de los estudiantes");
                            List<SeleccionarTutoria> seleccion = db.SeleccionarTutorias.ToList();
                            foreach (var seleccionar1 in seleccion)
                            {
                                Console.WriteLine(seleccionar1.Estudiante);
                            }
                        }
                    }
                }
                else
                {
                    Console.WriteLine("No existe usuario");
                    LoginAdministrador();
                }
            }
        }
    }
}